# Get Your Guide Coding Task

## Libraries used

[RxJava](https://github.com/ReactiveX/RxJava)/[RxAndroid](https://github.com/ReactiveX/RxAndroid) for asynchronous work. 

[Koin](https://github.com/InsertKoinIO/koin) for dependency injection

[CircleImageView](https://github.com/hdodenhof/CircleImageView) for circle imageview without using glide transformations

[Android Paging Library](https://developer.android.com/topic/libraries/architecture/paging) for the straightforward pagination

