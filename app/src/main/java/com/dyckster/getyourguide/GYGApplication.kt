package com.dyckster.getyourguide

import android.app.Application
import com.dyckster.getyourguide.di.Modules
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin

class GYGApplication : Application() {

    override fun onCreate() {
        super.onCreate()

        startKoin {
            androidLogger()
            androidContext(this@GYGApplication)
            modules(Modules.values())
        }
    }
}