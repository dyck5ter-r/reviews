package com.dyckster.getyourguide.presentation.reviewlist.adapter

import android.view.ViewGroup
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.dyckster.getyourguide.data.model.ERROR
import com.dyckster.getyourguide.data.model.LOADING
import com.dyckster.getyourguide.data.model.LoadingState
import com.dyckster.getyourguide.domain.model.Review

class ReviewListAdapter(private val reviewClickListener: (Review) -> Unit) :
    PagedListAdapter<Review, RecyclerView.ViewHolder>(newsDiffCallback) {

    var tryAgainListener: (() -> Unit)? = null
    var currentState: LoadingState = LOADING(true)
        set(value) {
            field = value
            notifyItemChanged(super.getItemCount())
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            ITEM_VIEW_TYPE -> ReviewViewHolder(parent, reviewClickListener)
            FOOTER_VIEW_TYPE -> ReviewFooterViewHolder(parent)
            else -> throw IllegalArgumentException("No view holder for view type: $viewType")
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is ReviewViewHolder -> getItem(position)?.let { holder.bind(it) }
            is ReviewFooterViewHolder -> holder.bind(currentState, tryAgainListener)
        }
    }

    override fun getItemViewType(position: Int) =
        if (position < super.getItemCount()) ITEM_VIEW_TYPE else FOOTER_VIEW_TYPE


    override fun getItemCount() = super.getItemCount() + if (hasFooter()) 1 else 0

    private fun hasFooter() =
        super.getItemCount() != 0 && (currentState is LOADING || currentState is ERROR)

    companion object {

        private const val ITEM_VIEW_TYPE = 1
        private const val FOOTER_VIEW_TYPE = 2

        private val newsDiffCallback = object : DiffUtil.ItemCallback<Review>() {

            override fun areItemsTheSame(oldItem: Review, newItem: Review): Boolean {
                return oldItem.id == newItem.id
            }

            override fun areContentsTheSame(oldItem: Review, newItem: Review): Boolean {
                return oldItem == newItem
            }

        }
    }

}