package com.dyckster.getyourguide.presentation

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.dyckster.getyourguide.R
import com.dyckster.getyourguide.presentation.reviewlist.ReviewListFragment

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                .replace(R.id.mainFrame, ReviewListFragment())
                .commitNow()
        }
    }
}
