package com.dyckster.getyourguide.presentation.reviewlist.adapter

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.dyckster.getyourguide.R
import com.dyckster.getyourguide.domain.model.Review
import com.dyckster.getyourguide.extensions.changeVisibility
import com.dyckster.getyourguide.extensions.getString
import com.dyckster.getyourguide.extensions.inflate
import com.dyckster.getyourguide.extensions.onClick
import kotlinx.android.synthetic.main.item_review.view.*
import java.text.SimpleDateFormat
import java.util.*

class ReviewViewHolder(parent: ViewGroup, private val listener: (Review) -> Unit) :
    RecyclerView.ViewHolder(parent.inflate(R.layout.item_review)) {

    private val dateFormat by lazy { SimpleDateFormat("EEEE, dd MMMM, yyyy", Locale.getDefault()) }

    fun bind(review: Review) {
        with(itemView) {
            onClick { listener.invoke(review) }
            val hasCountry = !review.author.country.isNullOrBlank()
            reviewTitle.changeVisibility(review.title.isNotEmpty())
            reviewTitle.text = review.title
            reviewText.changeVisibility(review.message.isNotEmpty())
            reviewText.text = review.message
            reviewRating.progress = review.rating
            Glide.with(this.context)
                .load(review.author.photoUrl)
                .placeholder(R.drawable.drawable_avatar_placeholder)
                .into(authorAvatar)
            authorOrigin.text = if (hasCountry) getString(
                R.string.reviewed_by_format,
                review.author.fullName,
                review.author.country!!
            ) else {
                review.author.fullName
            }
            reviewDate.text = dateFormat.format(review.created)
        }
    }
}