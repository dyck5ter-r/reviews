package com.dyckster.getyourguide.presentation.reviewdetail

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.dyckster.getyourguide.domain.model.Review
import com.dyckster.getyourguide.domain.repository.ReviewsRepository
import com.dyckster.getyourguide.presentation.base.BaseViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class ReviewDetailedViewModel(private val reviewsRepository: ReviewsRepository) : BaseViewModel() {

    private val reviewData = MutableLiveData<Review>()

    fun fetchReview(reviewId: Long) {
        reviewsRepository.getReview(reviewId)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                reviewData.postValue(it)
            }, {
            })
            .also { disposable.add(it) }
    }

    fun getReview(): LiveData<Review> = reviewData

}