package com.dyckster.getyourguide.presentation.reviewlist

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import com.dyckster.getyourguide.data.datasource.ReviewPagedDataSource
import com.dyckster.getyourguide.data.datasource.ReviewPagedDataSourceFactory
import com.dyckster.getyourguide.data.model.LoadingState
import com.dyckster.getyourguide.domain.model.Review
import com.dyckster.getyourguide.domain.model.SortOrder
import com.dyckster.getyourguide.domain.model.SortType
import com.dyckster.getyourguide.domain.repository.ReviewsRepository
import com.dyckster.getyourguide.presentation.base.BaseViewModel

class ReviewListViewModel(reviewsRepository: ReviewsRepository) : BaseViewModel() {

    private val dataSourceFactory: ReviewPagedDataSourceFactory
    private val currentSortType = MutableLiveData<SortType>()
    private val currentSortOrder = MutableLiveData<SortOrder>()
    var reviewsList: LiveData<PagedList<Review>>

    init {
        currentSortType.value = DEFAULT_SORT_TYPE
        currentSortOrder.value = DEFAULT_SORT_ORDER

        dataSourceFactory = ReviewPagedDataSourceFactory(
            HARDCODED_ACTIVITY_ID,
            DEFAULT_SORT_TYPE,
            DEFAULT_SORT_ORDER,
            disposable,
            reviewsRepository
        )
        reviewsList = LivePagedListBuilder<Int, Review>(dataSourceFactory, pagedConfig)
            .build()

        currentSortOrder.observeForever { dataSourceFactory.changeSortOrder(it) }
        currentSortType.observeForever { dataSourceFactory.changeSortType(it) }
    }


    fun switchSortOrder() {
        val newSortOrder = currentSortOrder.value?.switch()
        currentSortOrder.postValue(newSortOrder)
    }

    fun switchSortType(sortType: SortType) {
        if (sortType == currentSortType.value) return
        currentSortType.postValue(sortType)
    }

    fun refresh() {
        reviewsList = LivePagedListBuilder<Int, Review>(dataSourceFactory, pagedConfig)
            .build()
        dataSourceFactory.getReviewsDataSource().value?.invalidate()
    }

    fun retry() = dataSourceFactory.getReviewsDataSource().value?.retry()

    fun getLoadingState(): LiveData<LoadingState> = Transformations.switchMap<ReviewPagedDataSource,
            LoadingState>(
        dataSourceFactory.getReviewsDataSource(), ReviewPagedDataSource::getLoadingState
    )

    fun getSortType(): LiveData<SortType> = currentSortType
    fun getSortOrder(): LiveData<SortOrder> = currentSortOrder

    companion object {

        private const val PAGE_SIZE = 10
        private const val HARDCODED_ACTIVITY_ID = 23776L

        private val DEFAULT_SORT_ORDER = SortOrder.DESCENDING
        private val DEFAULT_SORT_TYPE = SortType.RATING

        private val pagedConfig = PagedList.Config.Builder()
            .setPageSize(PAGE_SIZE)
            .setInitialLoadSizeHint(PAGE_SIZE)
            .setEnablePlaceholders(false)
            .build()
    }


}