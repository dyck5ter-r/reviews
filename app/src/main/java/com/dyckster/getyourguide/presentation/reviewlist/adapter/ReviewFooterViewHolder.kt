package com.dyckster.getyourguide.presentation.reviewlist.adapter

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.dyckster.getyourguide.R
import com.dyckster.getyourguide.data.model.ERROR
import com.dyckster.getyourguide.data.model.LOADING
import com.dyckster.getyourguide.data.model.LoadingState
import com.dyckster.getyourguide.extensions.changeVisibility
import com.dyckster.getyourguide.extensions.inflate
import com.dyckster.getyourguide.extensions.onClick
import kotlinx.android.synthetic.main.item_review_footer.view.*

class ReviewFooterViewHolder(parent: ViewGroup) :
    RecyclerView.ViewHolder(parent.inflate(R.layout.item_review_footer)) {

    fun bind(state: LoadingState, tryAgainListener: (() -> Unit)?) {
        with(itemView) {
            tryAgainButton.onClick { tryAgainListener?.invoke() }
            tryAgainGroup.changeVisibility(state is ERROR)
            progressBar.changeVisibility(state is LOADING)
        }
    }

}