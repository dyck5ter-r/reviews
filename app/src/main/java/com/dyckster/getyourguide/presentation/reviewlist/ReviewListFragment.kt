package com.dyckster.getyourguide.presentation.reviewlist

import android.os.Bundle
import android.view.View
import android.view.animation.AccelerateDecelerateInterpolator
import android.widget.PopupMenu
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction.TRANSIT_FRAGMENT_CLOSE
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.RecyclerView
import com.dyckster.getyourguide.R
import com.dyckster.getyourguide.data.model.ERROR
import com.dyckster.getyourguide.data.model.LOADING
import com.dyckster.getyourguide.domain.model.SortOrder
import com.dyckster.getyourguide.domain.model.SortType
import com.dyckster.getyourguide.extensions.hide
import com.dyckster.getyourguide.extensions.onClick
import com.dyckster.getyourguide.extensions.show
import com.dyckster.getyourguide.presentation.reviewdetail.ReviewDetailedFragment
import com.dyckster.getyourguide.presentation.reviewlist.adapter.ReviewListAdapter
import kotlinx.android.synthetic.main.fragment_review_list.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class ReviewListFragment : Fragment(R.layout.fragment_review_list) {

    private var listScroll = 0
    private val viewModel: ReviewListViewModel by viewModel()
    private var adapter = ReviewListAdapter { openReviewDetailed(it.id) }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViewModels()
        adapter.tryAgainListener = { viewModel.retry() }
        reviewsList.adapter = adapter
        reviewsSwipeRefresh.setOnRefreshListener {
            viewModel.refresh()
            switchAdapter()
        }
        sortText.onClick { openSortMenu() }
        sortIcon.onClick {
            switchAdapter()
            viewModel.switchSortOrder()
        }
        tryAgainButton.onClick {
            switchAdapter()
            errorGroup.hide()
            viewModel.retry()
        }
        scrollToTopFab.onClick {
            if (listScroll > LIST_SCROLL_TO_TOP_SMOOTH_THRESHOLD) {
                reviewsList.scrollToPosition(0)
            } else {
                reviewsList.smoothScrollToPosition(0)
            }
            resetScrollState()
        }
    }

    override fun onStart() {
        super.onStart()
        reviewsList.addOnScrollListener(scrollListener)
    }

    private fun initViewModels() {
        //Observe list changes and pagination
        viewModel.reviewsList.observe(this, Observer {
            adapter.submitList(it)
        })
        //Observe loading state
        viewModel.getLoadingState().observe(this, Observer {
            val isEmpty = (it as? LOADING)?.isEmpty == true || (it as? ERROR)?.isEmpty == true
            reviewsSwipeRefresh.isRefreshing = isEmpty
            if (!isEmpty) {
                adapter.currentState = it
            } else {
                resetScrollState()
            }
            if (it is ERROR && isEmpty) {
                errorGroup.show()
            }
        })
        //Observe sort order changes (asc/desc)
        viewModel.getSortOrder().observe(this, Observer {
            sortIcon.animate()
                .setDuration(ARROW_ROTATE_DURATION)
                .setInterpolator(AccelerateDecelerateInterpolator())
                .rotation(if (it == SortOrder.ASCENDING) 180f else 0f)
        })
        //Observe sort type changes (rating/date)
        viewModel.getSortType().observe(this, Observer {
            sortText.setText(
                when (it!!) {
                    SortType.RATING -> R.string.title_sort_by_rating
                    SortType.DATE -> R.string.title_sort_by_date
                }
            )
        })
    }

    private fun openSortMenu() {
        val menu = PopupMenu(context, sortText)
        menu.inflate(R.menu.menu_sort_select)
        menu.setOnMenuItemClickListener {
            when (it.itemId) {
                R.id.sortDate -> viewModel.switchSortType(SortType.DATE)
                R.id.sortRate -> viewModel.switchSortType(SortType.RATING)
            }
            return@setOnMenuItemClickListener true
        }
        menu.show()
    }

    private fun switchAdapter() {
        adapter = ReviewListAdapter { openReviewDetailed(it.id) }
        adapter.tryAgainListener = {
            viewModel.retry()
        }
        reviewsList.swapAdapter(adapter, false)
    }

    private fun openReviewDetailed(reviewId: Long) {
        fragmentManager?.beginTransaction()
            ?.setReorderingAllowed(true)
            ?.replace(R.id.mainFrame, ReviewDetailedFragment.newInstance(reviewId))
            ?.addToBackStack(null)
            ?.setTransition(TRANSIT_FRAGMENT_CLOSE)
            ?.commit()
    }

    private fun resetScrollState() {
        listScroll = 0
        scrollToTopFab.hide()
    }

    override fun onStop() {
        super.onStop()
        reviewsList.removeOnScrollListener(scrollListener)
    }

    private val scrollListener = object : RecyclerView.OnScrollListener() {
        override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
            super.onScrolled(recyclerView, dx, dy)
            listScroll = (listScroll + dy).coerceAtLeast(0)
            if (listScroll > SHOW_FAB_SCROLL_THRESHOLD) {
                scrollToTopFab.show()
            } else {
                scrollToTopFab.hide()
            }
        }
    }

    companion object {
        private const val SHOW_FAB_SCROLL_THRESHOLD = 1000
        private const val LIST_SCROLL_TO_TOP_SMOOTH_THRESHOLD = 35000
        private const val ARROW_ROTATE_DURATION = 150L
    }

}