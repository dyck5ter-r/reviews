package com.dyckster.getyourguide.presentation.reviewdetail

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import com.bumptech.glide.Glide
import com.dyckster.getyourguide.R
import com.dyckster.getyourguide.domain.model.Review
import com.dyckster.getyourguide.domain.model.TravelerType
import com.dyckster.getyourguide.extensions.argument
import com.dyckster.getyourguide.extensions.changeVisibility
import com.dyckster.getyourguide.extensions.onClick
import kotlinx.android.synthetic.main.fragment_review_detailed.*
import org.koin.androidx.viewmodel.ext.android.viewModel
import java.text.SimpleDateFormat
import java.util.*

private const val ARGUMENT_REVIEW_ID = "review_id"

class ReviewDetailedFragment : Fragment(R.layout.fragment_review_detailed) {

    private val dateFormat by lazy { SimpleDateFormat("EEEE, dd MMMM, yyyy", Locale.getDefault()) }
    private val viewModel: ReviewDetailedViewModel by viewModel()
    private val reviewId: Long by argument(ARGUMENT_REVIEW_ID)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel.fetchReview(reviewId)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        backButton.onClick { activity?.onBackPressed() }
        initViewModel()
    }

    private fun initViewModel() =
        viewModel.getReview().observe(this, Observer { fillItDetails(it) })

    private fun fillItDetails(review: Review) {
        with(review) {
            val hasCountry = !author.country.isNullOrBlank()

            Glide.with(this@ReviewDetailedFragment)
                .load(author.photoUrl)
                .placeholder(R.drawable.drawable_avatar_placeholder)
                .into(authorAvatar)
            authorOrigin.text = if (hasCountry) getString(
                R.string.reviewed_by_format,
                review.author.fullName,
                review.author.country!!
            ) else {
                review.author.fullName
            }

            titleText.changeVisibility(title.isNotEmpty())
            titleText.text = title

            enjoymentGroup.changeVisibility(enjoyment.isNotEmpty())
            enjoymentText.text = enjoyment

            messageGroup.changeVisibility(message.isNotEmpty())
            messageText.text = message

            reviewRating.progress = review.rating

            reviewDate.text = dateFormat.format(created)

            travelerTypeText.changeVisibility(travelerType != TravelerType.UNKNOWN)
            travelerTypeText.setText(
                when (review.travelerType) {
                    TravelerType.SOLO -> R.string.traveler_solo
                    TravelerType.COUPLE -> R.string.traveler_couple
                    TravelerType.YOUNG_FAMILY -> R.string.traveler_young_fam
                    TravelerType.OLD_FAMILY -> R.string.traveler_old_fam
                    TravelerType.FRIENDS -> R.string.traveler_friends
                    TravelerType.UNKNOWN -> R.string.traveler_unknown
                }
            )
        }
    }

    companion object {

        fun newInstance(reviewId: Long): ReviewDetailedFragment {
            val fragment = ReviewDetailedFragment()
            val bundle = Bundle()
            bundle.putLong(ARGUMENT_REVIEW_ID, reviewId)
            fragment.arguments = bundle

            return fragment
        }

    }

}