package com.dyckster.getyourguide.extensions

import android.content.Context
import android.util.TypedValue
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.ViewTreeObserver
import android.view.inputmethod.InputMethodManager
import android.widget.CompoundButton

const val CLICK_DELAY = 250L
var lastClickTime: Long = 0L

fun View.onClick(action: () -> Unit) {
    setOnClickListener {
        if (System.currentTimeMillis() - lastClickTime > CLICK_DELAY) {
            lastClickTime = System.currentTimeMillis()
            action()
        }
    }
}

fun View.show() {
    changeVisibility(true)
}

fun View.hide() {
    changeVisibility(false)
}

fun CompoundButton.switch() {
    isChecked = !isChecked
}

fun View.changeVisibility(show: Boolean) {
    visibility = if (show) View.VISIBLE else View.GONE
}

fun ViewGroup.inflate(layoutRes: Int): View =
    LayoutInflater.from(context).inflate(layoutRes, this, false)

fun View.hideKeyboard() {
    val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
    imm.hideSoftInputFromWindow(windowToken, 0)
}

fun View.showKeyboard(showType: Int = InputMethodManager.SHOW_IMPLICIT) {
    val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
    imm.toggleSoftInput(showType, InputMethodManager.HIDE_IMPLICIT_ONLY)
}

fun Int.toPx(context: Context?): Float {
    if (context == null) return 0F
    return TypedValue.applyDimension(
        TypedValue.COMPLEX_UNIT_DIP,
        this.toFloat(),
        context.resources.displayMetrics
    ).toInt().toFloat()
}

fun Float.toPx(context: Context?): Float {
    if (context == null) return 0F
    return TypedValue.applyDimension(
        TypedValue.COMPLEX_UNIT_DIP,
        this,
        context.resources.displayMetrics
    ).toInt().toFloat()
}

inline fun <reified T : View> T.afterMeasured(crossinline f: T.() -> Unit) {
    viewTreeObserver.addOnGlobalLayoutListener(object : ViewTreeObserver.OnGlobalLayoutListener {
        override fun onGlobalLayout() {
            viewTreeObserver.removeOnGlobalLayoutListener(this)
            f()
        }
    })
}
