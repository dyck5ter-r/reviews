package com.dyckster.getyourguide.di

import com.dyckster.getyourguide.BuildConfig
import com.dyckster.getyourguide.data.mapper.AuthorMapper
import com.dyckster.getyourguide.data.mapper.ReviewMapper
import com.dyckster.getyourguide.data.mapper.TravelerTypeMapper
import com.dyckster.getyourguide.data.network.TravelApi
import com.dyckster.getyourguide.data.network.UserAgentInterceptor
import com.dyckster.getyourguide.data.repository.AppReviewRepository
import com.dyckster.getyourguide.domain.repository.ReviewsRepository
import com.dyckster.getyourguide.presentation.reviewdetail.ReviewDetailedViewModel
import com.dyckster.getyourguide.presentation.reviewlist.ReviewListViewModel
import io.reactivex.schedulers.Schedulers
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory


object Modules {

    private val appModule = module {
        viewModel { ReviewListViewModel(get()) }
        viewModel { ReviewDetailedViewModel(get()) }
    }

    private val dataSourceModule = module {
        single<ReviewsRepository> { AppReviewRepository(get(), get<ReviewMapper>()) }
    }

    private val networkModule = module {

        single {
            val logging = HttpLoggingInterceptor()
            logging.level = HttpLoggingInterceptor.Level.BODY
            logging
        }

        single { UserAgentInterceptor(BuildConfig.USER_AGENT) }

        single<OkHttpClient> {
            val httpClient = OkHttpClient.Builder()
            httpClient.addInterceptor(get<HttpLoggingInterceptor>())
            httpClient.addInterceptor(get<UserAgentInterceptor>())
            httpClient.build()

        }

        single<TravelApi> {
            Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.createWithScheduler(Schedulers.io()))
                .client(get())
                .baseUrl(BuildConfig.BASE_URL)
                .build()
                .create(TravelApi::class.java)
        }
    }

    private val mapperModule = module {
        single { ReviewMapper(get<AuthorMapper>(), get<TravelerTypeMapper>()) }
        single { AuthorMapper() }
        single { TravelerTypeMapper() }
    }

    fun values() = listOf(appModule, dataSourceModule, networkModule, mapperModule)
}