package com.dyckster.getyourguide.domain.model

enum class SortOrder {
    ASCENDING, DESCENDING;

    fun switch(): SortOrder {
        return if (this == ASCENDING) {
            DESCENDING
        } else {
            ASCENDING
        }
    }
}