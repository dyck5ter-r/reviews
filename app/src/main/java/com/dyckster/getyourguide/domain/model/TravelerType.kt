package com.dyckster.getyourguide.domain.model

enum class TravelerType {
    SOLO,
    COUPLE,
    YOUNG_FAMILY,
    OLD_FAMILY,
    FRIENDS,
    UNKNOWN
}