package com.dyckster.getyourguide.domain.model

data class ReviewAuthor(
    val fullName: String,
    val country: String?,
    val photoUrl: String?
)