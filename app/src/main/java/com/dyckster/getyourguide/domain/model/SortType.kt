package com.dyckster.getyourguide.domain.model

enum class SortType { RATING, DATE }