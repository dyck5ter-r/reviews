package com.dyckster.getyourguide.domain.model

import java.util.*

data class Review(
    val id: Long,
    val author: ReviewAuthor,
    val title: String,
    val message: String,
    val enjoyment: String,
    val isAnonymous: Boolean,
    val rating: Int,
    val created: Date,
    val language: String,
    val travelerType: TravelerType
)
