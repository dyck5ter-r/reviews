package com.dyckster.getyourguide.domain.repository

import com.dyckster.getyourguide.domain.model.Review
import com.dyckster.getyourguide.domain.model.SortOrder
import com.dyckster.getyourguide.domain.model.SortType
import io.reactivex.Single

interface ReviewsRepository {

    fun getReviews(
        activityId: Long,
        offset: Int,
        limit: Int,
        sortOrder: SortOrder,
        sortType: SortType
    ): Single<List<Review>>

    fun getReview(reviewId: Long): Single<Review>

}