package com.dyckster.getyourguide.data.model

import com.google.gson.annotations.SerializedName

data class ReviewsResponseModel(
    @SerializedName("reviews")
    val reviews: List<ReviewModel>,
    @SerializedName("totalCount")
    val totalCount: Int,
    @SerializedName("averageRating")
    val averageRating: Double,
    @SerializedName("pagination")
    val paginationInfo: PaginationModel
)

data class PaginationModel(
    val limit: Int,
    val offset: Int
)