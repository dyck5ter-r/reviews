package com.dyckster.getyourguide.data.mapper

import com.dyckster.getyourguide.data.model.ReviewAuthorModel
import com.dyckster.getyourguide.data.util.BaseMapper
import com.dyckster.getyourguide.domain.model.ReviewAuthor

class AuthorMapper : BaseMapper<ReviewAuthorModel, ReviewAuthor>() {

    override fun transform(obj: ReviewAuthorModel): ReviewAuthor {
        return ReviewAuthor(
            fullName = obj.fullName,
            country = obj.country,
            photoUrl = obj.photoUrl
        )
    }

}