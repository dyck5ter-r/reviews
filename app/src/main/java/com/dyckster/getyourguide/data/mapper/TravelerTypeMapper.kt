package com.dyckster.getyourguide.data.mapper

import com.dyckster.getyourguide.data.util.BaseMapper
import com.dyckster.getyourguide.domain.model.TravelerType

class TravelerTypeMapper : BaseMapper<String?, TravelerType>() {

    override fun transform(obj: String?): TravelerType {
        return when (obj) {
            "solo" -> TravelerType.SOLO
            "couple" -> TravelerType.COUPLE
            "young family" -> TravelerType.YOUNG_FAMILY
            "old family" -> TravelerType.OLD_FAMILY
            "friends" -> TravelerType.FRIENDS
            else -> TravelerType.UNKNOWN
        }
    }

}