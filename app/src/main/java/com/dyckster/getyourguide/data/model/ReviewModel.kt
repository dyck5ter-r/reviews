package com.dyckster.getyourguide.data.model

import java.util.*

data class ReviewModel(
    val id: Long,
    val author: ReviewAuthorModel,
    val title: String,
    val message: String,
    val enjoyment: String,
    val isAnonymous: Boolean,
    val rating: Int,
    val created: Date,
    val language: String,
    val travelerType: String?
)