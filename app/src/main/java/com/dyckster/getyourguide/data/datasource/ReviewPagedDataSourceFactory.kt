package com.dyckster.getyourguide.data.datasource

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.paging.DataSource
import com.dyckster.getyourguide.domain.model.Review
import com.dyckster.getyourguide.domain.model.SortOrder
import com.dyckster.getyourguide.domain.model.SortType
import com.dyckster.getyourguide.domain.repository.ReviewsRepository
import io.reactivex.disposables.CompositeDisposable

class ReviewPagedDataSourceFactory(
    private val activityId: Long,
    private var sortType: SortType,
    private var sortOrder: SortOrder,
    private val compositeDisposable: CompositeDisposable,
    private val reviewRepository: ReviewsRepository
) : DataSource.Factory<Int, Review>() {

    private val reviewsDataSourceLiveData = MutableLiveData<ReviewPagedDataSource>()

    override fun create(): DataSource<Int, Review> {
        val dataSource = ReviewPagedDataSource(
            activityId,
            sortType,
            sortOrder,
            compositeDisposable,
            reviewRepository
        )
        reviewsDataSourceLiveData.postValue(dataSource)
        return dataSource
    }

    fun changeSortOrder(sortOrder: SortOrder) {
        this.sortOrder = sortOrder
        reviewsDataSourceLiveData.value?.invalidate()
    }

    fun changeSortType(sortType: SortType) {
        this.sortType = sortType
        reviewsDataSourceLiveData.value?.invalidate()
    }

    fun getReviewsDataSource() =
        reviewsDataSourceLiveData as LiveData<ReviewPagedDataSource>
}
