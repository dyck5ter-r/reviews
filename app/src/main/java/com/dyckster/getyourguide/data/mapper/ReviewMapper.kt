package com.dyckster.getyourguide.data.mapper

import com.dyckster.getyourguide.data.model.ReviewAuthorModel
import com.dyckster.getyourguide.data.model.ReviewModel
import com.dyckster.getyourguide.data.util.BaseMapper
import com.dyckster.getyourguide.domain.model.Review
import com.dyckster.getyourguide.domain.model.ReviewAuthor
import com.dyckster.getyourguide.domain.model.TravelerType

class ReviewMapper(
    private val authorMapper: BaseMapper<ReviewAuthorModel, ReviewAuthor>,
    private val travelerTypeMapper: BaseMapper<String?, TravelerType>
) : BaseMapper<ReviewModel, Review>() {

    override fun transform(obj: ReviewModel): Review {
        return Review(
            id = obj.id,
            author = authorMapper.transform(obj.author),
            title = obj.title,
            message = obj.message,
            enjoyment = obj.enjoyment,
            isAnonymous = obj.isAnonymous,
            rating = obj.rating,
            created = obj.created,
            language = obj.language,
            travelerType = travelerTypeMapper.transform(obj.travelerType)
        )
    }

}