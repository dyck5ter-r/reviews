package com.dyckster.getyourguide.data.network

import com.dyckster.getyourguide.data.model.ReviewsResponseModel
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface TravelApi {

    @GET("activities/{activityId}/reviews")
    fun getReviews(
        @Path("activityId") activityId: Long,
        @Query("offset") offset: Int,
        @Query("limit") limit: Int,
        @Query("sort") sort: String
    ): Single<ReviewsResponseModel>

}