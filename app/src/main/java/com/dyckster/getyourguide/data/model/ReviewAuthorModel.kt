package com.dyckster.getyourguide.data.model

import com.google.gson.annotations.SerializedName

data class ReviewAuthorModel(
    val fullName: String,
    val country: String?,
    @SerializedName("photo")
    val photoUrl: String?
)