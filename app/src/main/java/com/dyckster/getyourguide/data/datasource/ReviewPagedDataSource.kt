package com.dyckster.getyourguide.data.datasource

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.paging.PageKeyedDataSource
import com.dyckster.getyourguide.data.model.DONE
import com.dyckster.getyourguide.data.model.ERROR
import com.dyckster.getyourguide.data.model.LOADING
import com.dyckster.getyourguide.data.model.LoadingState
import com.dyckster.getyourguide.domain.model.Review
import com.dyckster.getyourguide.domain.model.SortOrder
import com.dyckster.getyourguide.domain.model.SortType
import com.dyckster.getyourguide.domain.repository.ReviewsRepository
import io.reactivex.Completable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.functions.Action
import io.reactivex.schedulers.Schedulers

private const val TAG = "ReviewPagerSource"

class ReviewPagedDataSource(
    private val activityId: Long,
    private val sortType: SortType,
    private val sortOrder: SortOrder,
    private val disposable: CompositeDisposable,
    private val reviewsRepository: ReviewsRepository
) : PageKeyedDataSource<Int, Review>() {

    private var loadingState: MutableLiveData<LoadingState> = MutableLiveData()
    private var retryCompletable: Completable? = null

    override fun loadInitial(
        params: LoadInitialParams<Int>,
        callback: LoadInitialCallback<Int, Review>
    ) {
        val initialOffset = 0
        updateState(LOADING(true))
        reviewsRepository.getReviews(
            activityId,
            initialOffset,
            params.requestedLoadSize,
            sortOrder,
            sortType
        )
            .subscribe({
                updateState(LOADING(it.isEmpty()))
                callback.onResult(it, initialOffset, it.size)
            }, {
                Log.e(TAG, "Failed to fetch initial list", it)
                updateState(ERROR(true))
                setRetry(Action { loadInitial(params, callback) })
            })
            .also { disposable.add(it) }
    }

    override fun loadAfter(params: LoadParams<Int>, callback: LoadCallback<Int, Review>) {
        updateState(LOADING(false))
        reviewsRepository.getReviews(
            activityId,
            params.key,
            params.requestedLoadSize,
            sortOrder,
            sortType
        )
            .subscribe({
                updateState(DONE)
                callback.onResult(it, params.key + it.size)
            }, {
                Log.e(TAG, "Failed to fetch list", it)
                updateState(ERROR(false))
                setRetry(Action { loadAfter(params, callback) })
            })
            .also { disposable.add(it) }
    }

    override fun loadBefore(params: LoadParams<Int>, callback: LoadCallback<Int, Review>) = Unit

    private fun updateState(state: LoadingState) = loadingState.postValue(state)

    private fun setRetry(action: Action?) {
        retryCompletable = action?.let { Completable.fromAction(it) }
    }

    fun retry() {
        retryCompletable?.also {
            it.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe()
                .also { d -> disposable.add(d) }
        }
    }


    fun getLoadingState(): LiveData<LoadingState> = loadingState


}