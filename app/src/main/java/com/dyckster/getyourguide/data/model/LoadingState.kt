package com.dyckster.getyourguide.data.model

sealed class LoadingState

data class LOADING(val isEmpty: Boolean = true) : LoadingState()
data class ERROR(val isEmpty: Boolean = true) : LoadingState()
object DONE : LoadingState()

