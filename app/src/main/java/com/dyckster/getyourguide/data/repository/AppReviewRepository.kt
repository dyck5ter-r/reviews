package com.dyckster.getyourguide.data.repository

import com.dyckster.getyourguide.data.model.ReviewModel
import com.dyckster.getyourguide.data.network.TravelApi
import com.dyckster.getyourguide.data.util.BaseMapper
import com.dyckster.getyourguide.domain.model.Review
import com.dyckster.getyourguide.domain.model.SortOrder
import com.dyckster.getyourguide.domain.model.SortType
import com.dyckster.getyourguide.domain.repository.ReviewsRepository
import io.reactivex.Single

class AppReviewRepository(
    private val travelApi: TravelApi,
    private val reviewMapper: BaseMapper<ReviewModel, Review>
) : ReviewsRepository {

    private val reviews: MutableMap<Long, Review> = HashMap()

    override fun getReviews(
        activityId: Long,
        offset: Int,
        limit: Int,
        sortOrder: SortOrder,
        sortType: SortType
    ): Single<List<Review>> {
        val sortTypeString = when (sortType) {
            SortType.RATING -> "rating"
            SortType.DATE -> "date"
        }
        val sortOrderString = when (sortOrder) {
            SortOrder.ASCENDING -> "asc"
            SortOrder.DESCENDING -> "desc"
        }
        return travelApi.getReviews(
            activityId,
            offset,
            limit,
            "${sortTypeString}:${sortOrderString}"
        )
            .map { reviewMapper.transform(it.reviews) }
            .doOnSuccess { reviews.putAll(it.associateBy { rev -> rev.id }) }
    }

    override fun getReview(reviewId: Long): Single<Review> =
        Single.just(reviews[reviewId])

}