package com.dyckster.getyourguide.presentation.reviewdetail

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.dyckster.getyourguide.data.util.ImmediateScheduler
import com.dyckster.getyourguide.data.util.TravelFactory
import com.dyckster.getyourguide.domain.model.Review
import com.dyckster.getyourguide.domain.repository.ReviewsRepository
import io.reactivex.Single
import io.reactivex.android.plugins.RxAndroidPlugins
import io.reactivex.plugins.RxJavaPlugins
import junit.framework.Assert.assertNotNull
import junit.framework.Assert.assertTrue
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito.*
import org.mockito.MockitoAnnotations

class ReviewDetailedViewModelTest {

    @Mock
    lateinit var reviewsRepository: ReviewsRepository
    @Mock
    lateinit var observer: Observer<Review>
    @get:Rule
    val rule = InstantTaskExecutorRule()

    lateinit var viewModel: ReviewDetailedViewModel

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)

        RxJavaPlugins.setIoSchedulerHandler { ImmediateScheduler() }
        RxAndroidPlugins.setInitMainThreadSchedulerHandler { ImmediateScheduler() }

        viewModel = ReviewDetailedViewModel(reviewsRepository)
        viewModel.getReview().observeForever(observer)
    }

    @Test
    fun `test null`() {
        `when`(reviewsRepository.getReview(anyLong())).thenReturn(null)
        assertNotNull(viewModel.getReview())
        assertTrue(viewModel.getReview().hasObservers())
    }

    @Test
    fun `test success`() {
        val review = TravelFactory.review(0)
        `when`(reviewsRepository.getReview(anyLong())).thenReturn(Single.just(review))
        viewModel.fetchReview(0)
        verify(observer).onChanged(review)
        assertNotNull(viewModel.getReview())
        assertTrue(viewModel.getReview().hasObservers())

    }


}