package com.dyckster.getyourguide.extensions

import android.app.Activity
import android.view.View
import android.widget.Switch
import com.ibm.icu.impl.Assert.fail
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.Robolectric
import org.robolectric.RobolectricTestRunner
import org.robolectric.android.controller.ActivityController
import java.lang.IllegalStateException

@RunWith(RobolectricTestRunner::class)
class ViewExtensionTest {

    private lateinit var activityController: ActivityController<Activity>
    private lateinit var activity: Activity

    private lateinit var sampleView: View

    @Before
    fun setUp() {
        activityController = Robolectric.buildActivity(Activity::class.java)
        activity = activityController.get()

        sampleView = View(activity)
    }

    @Test
    fun `visibility test`() {
        assert(sampleView.visibility == View.VISIBLE)
        sampleView.hide()
        assert(sampleView.visibility == View.GONE)
        sampleView.show()
        assert(sampleView.visibility == View.VISIBLE)
        sampleView.changeVisibility(false)
        assert(sampleView.visibility == View.GONE)
        sampleView.hide()
        assert(sampleView.visibility == View.GONE)
        sampleView.changeVisibility(true)
        assert(sampleView.visibility == View.VISIBLE)
    }

    @Test
    fun `compound switch`() {
        val compoundButton = Switch(activity)
        assert(!compoundButton.isChecked)
        compoundButton.switch()
        assert(compoundButton.isChecked)
        compoundButton.switch()
        assert(!compoundButton.isChecked)
    }

    @Test
    fun `on click`() {
        val v = View(activity)
        val v2 = View(activity)

        v.onClick { }
        v2.onClick { fail("Second view must not call click") }

        v.performClick()
        v2.performClick()
    }

    @Test(expected = IllegalStateException::class)
    fun `on click timer`() {
        val v = View(activity)
        val v2 = View(activity)

        v.onClick { }
        v2.onClick { fail("Second view must call click") }

        v.performClick()
        Thread.sleep(CLICK_DELAY)
        v2.performClick()
    }
}