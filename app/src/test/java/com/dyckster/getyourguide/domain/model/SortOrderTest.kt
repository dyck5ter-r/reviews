package com.dyckster.getyourguide.domain.model

import junit.framework.Assert.assertEquals
import org.junit.Test

class SortOrderTest {

    @Test
    fun `switch state`() {
        val ascToDesc = SortOrder.ASCENDING.switch()
        val descToAsc = SortOrder.DESCENDING.switch()

        assertEquals(SortOrder.DESCENDING, ascToDesc)
        assertEquals(SortOrder.ASCENDING, descToAsc)
    }

}