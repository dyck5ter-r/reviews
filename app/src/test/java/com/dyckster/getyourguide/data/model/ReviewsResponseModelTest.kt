package com.dyckster.getyourguide.data.model

import com.google.gson.Gson
import com.google.gson.internal.bind.DateTypeAdapter
import junit.framework.Assert.assertEquals
import org.junit.Test

class ReviewsResponseModelTest {

    @Test
    fun `json review list parse`() {
        val parsed = Gson().fromJson(jsonString, ReviewsResponseModel::class.java)
        val paginationModel = PaginationModel(5, 0)

        val expectedModel = ReviewsResponseModel(
            reviews = listOf(reviewModel()),
            totalCount = 1,
            averageRating = 5.0,
            paginationInfo = paginationModel
        )
        assertEquals(expectedModel, parsed)
        assertEquals(expectedModel.reviews, parsed.reviews)
        assertEquals(1, parsed.reviews.size)
        assertEquals(1, parsed.totalCount)
        assertEquals(5.0, parsed.averageRating)
        assertEquals(paginationModel, parsed.paginationInfo)
    }

    @Test
    fun `json pagination  parse`() {
        val parsed = Gson().fromJson(paginationJson, PaginationModel::class.java)
        assertEquals(5, parsed.limit)
        assertEquals(0, parsed.offset)
    }

    private val jsonString = "{\n" +
            "\"reviews\": [{\n" +
            "\"id\": 11704585,\n" +
            "\"author\": {\n" +
            "\"fullName\": \"John Doe\",\n" +
            "\"country\": \"Germany\"\n" +
            "},\n" +
            "\"title\": \"\",\n" +
            "\"message\": \"Great tour, such an amazing building and great tour guide!\",\n" +
            "\"enjoyment\": \"The scale of the building\",\n" +
            "\"isAnonymous\": false,\n" +
            "\"rating\": 5,\n" +
            "\"created\": \"2020-02-04T13:40:36+03:00\",\n" +
            "\"language\": \"en\",\n" +
            "\"travelerType\": \"solo\"\n" +
            "}],\n" +
            "\"totalCount\": 1,\n" +
            "\"averageRating\": 5,\n" +
            "\"pagination\": {\n" +
            "\"limit\": 5,\n" +
            "\"offset\": 0\n" +
            "}\n" +
            "}"

    private fun reviewModel() = ReviewModel(
        11704585,
        ReviewAuthorModel("John Doe", "Germany", null),
        "",
        "Great tour, such an amazing building and great tour guide!",
        "The scale of the building",
        false,
        5,
        DateTypeAdapter().fromJson("\"2020-02-04T13:40:36+03:00\""),
        "en",
        "solo"
    )

    private val paginationJson = "{\n" +
            "\"limit\": 5,\n" +
            "\"offset\": 0\n" +
            "}"

}