package com.dyckster.getyourguide.data.model

import junit.framework.Assert.*
import org.junit.Assert.assertNotEquals
import org.junit.Test

class LoadingStateTest {

    @Test
    fun `loading state LOADING`() {
        val loadingFirst = LOADING(true)
        val loadingSecond = LOADING(true)
        val loadingThird = LOADING(false)

        assertEquals(loadingFirst, loadingSecond)
        assertEquals(loadingFirst.isEmpty, loadingSecond.isEmpty)
        assertNotSame(loadingFirst, loadingSecond)
        assertNotEquals(loadingSecond, loadingThird)
        assertNotEquals(loadingFirst, loadingThird)
        assertNotEquals(loadingSecond.isEmpty, loadingThird.isEmpty)
        assertNotEquals(loadingFirst.isEmpty, loadingThird.isEmpty)
    }

    @Test
    fun `loading state ERROR`() {
        val errorFirst = ERROR(true)
        val errorSecond = ERROR(true)
        val errorThird = ERROR(false)

        assertEquals(errorFirst, errorSecond)
        assertEquals(errorFirst.isEmpty, errorSecond.isEmpty)
        assertNotSame(errorFirst, errorSecond)
        assertNotEquals(errorSecond, errorThird)
        assertNotEquals(errorFirst, errorThird)
        assertNotEquals(errorSecond.isEmpty, errorThird.isEmpty)
        assertNotEquals(errorFirst.isEmpty, errorThird.isEmpty)
    }

    @Test
    fun `loading state DONE`() {
        val doneFirst = DONE
        val doneSecond = DONE

        assertEquals(doneFirst, doneSecond)
        assertSame(doneFirst, doneSecond)
    }

}