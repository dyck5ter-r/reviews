package com.dyckster.getyourguide.data.mapper

import com.dyckster.getyourguide.domain.model.TravelerType
import com.dyckster.getyourguide.domain.model.TravelerType.*
import junit.framework.Assert.assertEquals
import org.junit.Test

class TravelerTypeMapperTest {

    @Test
    fun `map solo`() = testMapping(listOf("solo", "SOLO", "many"), listOf(SOLO, UNKNOWN, UNKNOWN))

    @Test
    fun `map couple`() =
        testMapping(listOf("couple", "COUPLE", "coupl"), listOf(COUPLE, UNKNOWN, UNKNOWN))

    @Test
    fun `map young family`() =
        testMapping(
            listOf("young family", "YOUNG FAMILY", "young_family"),
            listOf(YOUNG_FAMILY, UNKNOWN, UNKNOWN)
        )

    @Test
    fun `map old family`() =
        testMapping(
            listOf("old family", "OLD FAMILY", "OLD family"),
            listOf(OLD_FAMILY, UNKNOWN, UNKNOWN)
        )

    @Test
    fun `map friends`() =
        testMapping(
            listOf("friends", "FRIENDS", "frrds"),
            listOf(FRIENDS, UNKNOWN, UNKNOWN)
        )

    private fun testMapping(inputs: List<String>, expectedResults: List<TravelerType>) {
        val mapper = TravelerTypeMapper()
        val results = mapper.transform(inputs)

        assertEquals(expectedResults, results)
    }
}