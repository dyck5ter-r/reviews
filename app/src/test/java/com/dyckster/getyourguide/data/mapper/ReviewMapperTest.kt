package com.dyckster.getyourguide.data.mapper

import com.dyckster.getyourguide.data.model.ReviewAuthorModel
import com.dyckster.getyourguide.data.model.ReviewModel
import com.dyckster.getyourguide.domain.model.ReviewAuthor
import com.dyckster.getyourguide.domain.model.TravelerType
import junit.framework.Assert.assertEquals
import org.junit.Test
import java.util.*

class ReviewMapperTest {

    private val authorMapper = AuthorMapper()
    private val travelerTypeMapper = TravelerTypeMapper()

    @Test
    fun `map normal review`() {
        val reviewMapper = ReviewMapper(authorMapper, travelerTypeMapper)
        val reviewModel = ReviewModel(
            0,
            ReviewAuthorModel("John Doe", "Germany", "image.png"),
            "Title",
            "Message",
            "",
            false,
            4,
            Date(),
            "en",
            "solo"
        )
        val mapped = reviewMapper.transform(reviewModel)

        with(mapped) {
            assertEquals(reviewModel.id, id)
            assertEquals(ReviewAuthor("John Doe", "Germany", "image.png"), author)
            assertEquals(reviewModel.title, title)
            assertEquals(reviewModel.message, message)
            assertEquals(reviewModel.enjoyment, enjoyment)
            assertEquals(reviewModel.isAnonymous, isAnonymous)
            assertEquals(reviewModel.rating, rating)
            assertEquals(reviewModel.created, created)
            assertEquals(reviewModel.language, language)
            assertEquals(TravelerType.SOLO, travelerType)
        }


    }

}