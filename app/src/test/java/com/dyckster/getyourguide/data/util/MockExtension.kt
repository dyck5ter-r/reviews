package com.dyckster.getyourguide.data.util

import org.mockito.Mockito

internal fun <T> anyObject(): T {
    return Mockito.anyObject<T>()
}