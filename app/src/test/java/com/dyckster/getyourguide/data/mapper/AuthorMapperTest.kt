package com.dyckster.getyourguide.data.mapper

import com.dyckster.getyourguide.data.model.ReviewAuthorModel
import com.google.gson.Gson
import junit.framework.Assert.assertEquals
import org.junit.Test

class AuthorMapperTest {

    @Test
    fun `map full author`() {
        val mapper = AuthorMapper()
        val authorModel = ReviewAuthorModel("John Doe", "Germany", "image.png")
        val author = mapper.transform(authorModel)

        assertEquals(authorModel.fullName, author.fullName)
        assertEquals(authorModel.country, author.country)
        assertEquals(authorModel.photoUrl, author.photoUrl)
    }

    @Test
    fun `map author without country`() {
        val mapper = AuthorMapper()
        val authorModel = ReviewAuthorModel("John Doe", null, "image.png")
        val author = mapper.transform(authorModel)

        assertEquals(authorModel.fullName, author.fullName)
        assertEquals(null, author.country)
        assertEquals(authorModel.photoUrl, author.photoUrl)
    }

    @Test
    fun `map author without country and image`() {
        val mapper = AuthorMapper()
        val authorModel = ReviewAuthorModel("John Doe", null, null)
        val author = mapper.transform(authorModel)

        assertEquals(authorModel.fullName, author.fullName)
        assertEquals(null, author.country)
        assertEquals(null, author.photoUrl)
    }

    @Test
    fun `map author without image`() {
        val mapper = AuthorMapper()
        val authorModel = ReviewAuthorModel("John Doe", "Germany", null)
        val author = mapper.transform(authorModel)

        assertEquals(authorModel.fullName, author.fullName)
        assertEquals(authorModel.country, author.country)
        assertEquals(null, author.photoUrl)
    }

    @Test(expected = IllegalArgumentException::class)
    fun `map null name`() {
        val mapper = AuthorMapper()
        val authorGson = Gson().fromJson(
            "{\n" +
                    "\"country\": \"Литва\",\n" +
                    "\"photo\": \"https://cdn.getyourguide.com/img/customer_img-15224055-4080850121-11.jpg\"\n" +
                    "}", ReviewAuthorModel::class.java
        )
        mapper.transform(authorGson)
    }
}