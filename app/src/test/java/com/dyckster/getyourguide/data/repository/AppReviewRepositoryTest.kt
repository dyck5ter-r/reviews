package com.dyckster.getyourguide.data.repository

import com.dyckster.getyourguide.data.mapper.AuthorMapper
import com.dyckster.getyourguide.data.mapper.ReviewMapper
import com.dyckster.getyourguide.data.mapper.TravelerTypeMapper
import com.dyckster.getyourguide.data.network.TravelApi
import com.dyckster.getyourguide.data.util.TravelFactory
import com.dyckster.getyourguide.domain.model.SortOrder
import com.dyckster.getyourguide.domain.model.SortType
import com.dyckster.getyourguide.domain.repository.ReviewsRepository
import io.reactivex.Single
import junit.framework.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito.*
import org.mockito.MockitoAnnotations

class AppReviewRepositoryTest {

    @Mock
    lateinit var rentalApi: TravelApi

    private val reviewMapper = ReviewMapper(AuthorMapper(), TravelerTypeMapper())

    private lateinit var repository: ReviewsRepository

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        repository = AppReviewRepository(rentalApi, reviewMapper)
    }

    @Test
    fun `load and map`() {
        val initModel = TravelFactory.reviewResponseModel()
        val models = reviewMapper.transform(initModel.reviews)
        `when`(rentalApi.getReviews(anyLong(), anyInt(), anyInt(), anyString()))
            .thenReturn(Single.just(initModel))

        val reviews =
            repository.getReviews(0, 0, 10, SortOrder.DESCENDING, SortType.RATING).blockingGet()
        assertEquals(models, reviews)
    }

    @Test
    fun `load map and find by id`() {
        val reviewId = 50L
        val initModel = TravelFactory.reviewResponseModel(reviewId)
        val models = reviewMapper.transform(initModel.reviews)
        `when`(rentalApi.getReviews(anyLong(), anyInt(), anyInt(), anyString()))
            .thenReturn(Single.just(initModel))

        val reviews =
            repository.getReviews(0, 0, 10, SortOrder.DESCENDING, SortType.RATING).blockingGet()
        val reviewDetailed = repository.getReview(reviewId).blockingGet()

        assertEquals(models, reviews)
        assertEquals(reviewId, reviewDetailed.id)
    }

    @Test
    fun `sort order desc and type rating`() {
        val initModel = TravelFactory.reviewResponseModel()
        `when`(rentalApi.getReviews(anyLong(), anyInt(), anyInt(), anyString()))
            .thenReturn(Single.just(initModel))

        repository.getReviews(0, 0, 10, SortOrder.DESCENDING, SortType.RATING)
        verify(rentalApi).getReviews(0, 0, 10, "rating:desc")
    }

    @Test
    fun `sort order asc and type rating`() {
        val initModel = TravelFactory.reviewResponseModel()
        `when`(rentalApi.getReviews(anyLong(), anyInt(), anyInt(), anyString()))
            .thenReturn(Single.just(initModel))

        repository.getReviews(0, 0, 10, SortOrder.ASCENDING, SortType.RATING)
        verify(rentalApi).getReviews(0, 0, 10, "rating:asc")
    }

    @Test
    fun `sort order desc and type date`() {
        val initModel = TravelFactory.reviewResponseModel()
        `when`(rentalApi.getReviews(anyLong(), anyInt(), anyInt(), anyString()))
            .thenReturn(Single.just(initModel))

        repository.getReviews(0, 0, 10, SortOrder.DESCENDING, SortType.DATE)
        verify(rentalApi).getReviews(0, 0, 10, "date:desc")
    }

    @Test
    fun `sort order asc and type date`() {
        val initModel = TravelFactory.reviewResponseModel()
        `when`(rentalApi.getReviews(anyLong(), anyInt(), anyInt(), anyString()))
            .thenReturn(Single.just(initModel))

        repository.getReviews(0, 0, 10, SortOrder.ASCENDING, SortType.DATE)
        verify(rentalApi).getReviews(0, 0, 10, "date:asc")
    }

}