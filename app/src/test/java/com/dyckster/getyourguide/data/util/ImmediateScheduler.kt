package com.dyckster.getyourguide.data.util

import io.reactivex.Scheduler
import io.reactivex.internal.schedulers.ExecutorScheduler
import java.util.concurrent.TimeUnit


class ImmediateScheduler : Scheduler() {

    override fun scheduleDirect(run: Runnable, delay: Long, unit: TimeUnit) = super.scheduleDirect(run, 0, unit)

    override fun createWorker() = ExecutorScheduler.ExecutorWorker(Runnable::run)
}