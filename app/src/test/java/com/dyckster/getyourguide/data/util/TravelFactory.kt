package com.dyckster.getyourguide.data.util

import com.dyckster.getyourguide.data.model.PaginationModel
import com.dyckster.getyourguide.data.model.ReviewAuthorModel
import com.dyckster.getyourguide.data.model.ReviewModel
import com.dyckster.getyourguide.data.model.ReviewsResponseModel
import com.dyckster.getyourguide.domain.model.Review
import com.dyckster.getyourguide.domain.model.ReviewAuthor
import com.dyckster.getyourguide.domain.model.TravelerType
import com.google.gson.internal.bind.DateTypeAdapter

object TravelFactory {

    fun reviewResponseModel(reviewId: Long = 10): ReviewsResponseModel {
        return ReviewsResponseModel(
            reviews = listOf(reviewModel(reviewId)),
            totalCount = 1,
            averageRating = 5.0,
            paginationInfo = PaginationModel(5, 0)
        )
    }

    fun reviewModel(id: Long): ReviewModel {
        return ReviewModel(
            id,
            ReviewAuthorModel("John Doe", "Germany", null),
            "",
            "Great tour, such an amazing building and great tour guide!",
            "The scale of the building",
            false,
            5,
            DateTypeAdapter().fromJson("\"2020-02-04T13:40:36+03:00\""),
            "en",
            "solo"
        )
    }

    fun review(id: Long): Review {
        return Review(
            id,
            ReviewAuthor("John Doe", "Germany", null),
            "",
            "Great tour, such an amazing building and great tour guide!",
            "The scale of the building",
            false,
            5,
            DateTypeAdapter().fromJson("\"2020-02-04T13:40:36+03:00\""),
            "en",
            TravelerType.SOLO
        )
    }

}